package ru.t1.kubatov.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

    void deleteTaskByID();

    void deleteTaskByIndex();

    void showTaskByID();

    void showTaskByIndex();

    void updateTaskByID();

    void updateTaskByIndex();

}
