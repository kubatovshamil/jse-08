package ru.t1.kubatov.tm.api.repository;

import ru.t1.kubatov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}