package ru.t1.kubatov.tm.api.service;

import ru.t1.kubatov.tm.api.repository.IProjectRepository;
import ru.t1.kubatov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

    Project updateByID(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
